# Farm Bot

A javafx app to help manage your farm. Written for CS 420 at UAB (Group E).

## Getting Started

Installing this app on your local machine from this repository is fairly simple and non trivial.

### Prerequisites

In order to run the source code, you will need the latest version of Java 8. Versions well below update 60 will not work, so be sure your version of Java is up to date.
A javafx scene editor was also used to create the FXML files, but is not required.

### Setup

This project was built using netbeans, so if using netbeans, no action should be required as `nbproject/` will handle any build instructions.

If another IDE is being used to build the application, make sure the application runs off the main class `farmbot.Main`. You should then be able to build the application.

## Contributors

* John Carver
* John Stephenson
* Nick Netterville
* Tyler Smith
* Uzma Nur