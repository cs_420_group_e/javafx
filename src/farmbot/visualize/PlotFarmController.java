package farmbot.visualize;

import farmbot.Component.Component;
import farmbot.Component.ItemContainer;
import farmbot.FarmBot;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.Group;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

/**
 * FXML Controller class
 *
 * @author Nick, Tyler Smith
 */
public class PlotFarmController implements Initializable {

    @FXML
    private Group group;
    
    private Double minY;
    private Double minX;
    private Double maxY;
    private Double maxX;
    
    private Double winHeight;
    private Double winLength;
    private double scale = 1.0;
    
    @FXML
    private AnchorPane anchorpane;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        // get window dimensions
        winHeight = anchorpane.getPrefHeight();
        winLength = anchorpane.getPrefWidth();
        
        // find scale
        findScale(FarmBot.getInstance().getRoot());
        // build canvas
        buildCanvas(FarmBot.getInstance().getRoot());
        
    }
    
    /**
     * Builds the canvas from the list of Items and ItemContainers
     * @param root the Item/Container
     * @param child the Item/Container casted into a TreeItem
     */
    private void buildCanvas(Component root) {
        for (Component item : ((ItemContainer)root).getList()) {
            if (item instanceof ItemContainer) {
                
            	Rectangle r1 = new Rectangle(item.getLocationX() * scale, item.getLocationY() * scale, item.getLength() * scale, item.getWidth() * scale);
                r1.setFill(Color.TRANSPARENT);	
                r1.setStroke(Color.BLACK);
                group.getChildren().add(r1);
                
                Text text = new Text(item.getLocationX() * scale, item.getLocationY() * scale, item.getName() + " (" + item.getLocationX() + ", " + item.getLocationY() + ", " + item.getLength() + ", " + item.getWidth() + ")");
                text.setTextAlignment(TextAlignment.CENTER); // this doesnt work
                group.getChildren().add(text);
            	
                
                buildCanvas(item);
            } else {
                
            	Rectangle r1 = new Rectangle(item.getLocationX() * scale, item.getLocationY()* scale, item.getLength() * scale, item.getWidth() * scale);	
                r1.setFill(Color.BLACK);
                r1.setStroke(Color.BLACK);
            	group.getChildren().add(r1);
                
                Text text = new Text(item.getLocationX() * scale, item.getLocationY()* scale, item.getName() + " (" + item.getLocationX() + ", " + item.getLocationY() + ", " + item.getLength() + ", " + item.getWidth() + ")");
                text.setTextAlignment(TextAlignment.CENTER); // this doesnt work
                group.getChildren().add(text);
            	
                
            }
        }
    }
    
    /**
     * finds the fit to screen scale for the visualization
     * such that the rectangles will fit optimally on the
     * provided screen.
     * @param root the Item/Container
     */
    private void findScale(Component root) {
        for (Component item : ((ItemContainer)root).getList()) {
            
                if (maxY == null) {
                    maxY = item.getLocationY() + item.getWidth();
                    maxX = item.getLocationX() + item.getLength();
                }
                if (maxY < item.getLocationY()) {
                    maxY = item.getLocationY() + item.getWidth();
                }
                if (maxX < item.getLocationX()) {
                    maxX = item.getLocationX() + item.getLength();
                }
                Double scaleLength = winLength / (maxX);
                Double scaleHeight = winHeight / (maxY);
                
                if (scaleHeight > scaleLength) {
                    scale = scaleLength;
                } else {
                    scale = scaleHeight;
                }
            
            if (item instanceof ItemContainer) {
                findScale(item);
            }
        }
    }

    
}
