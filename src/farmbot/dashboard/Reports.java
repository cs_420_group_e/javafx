package farmbot.dashboard;

import farmbot.FarmBot;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text; 
/**
 * Reports UI
 * @author Nick
 */
public class Reports{

    static Scene reportScene;
    
    public void init(Stage primaryStage){
        Text titleText = new Text("Reports");
        titleText.setFont(Font.font (24));
        
        Button btn = new Button();
        btn.setText("go back");
        
        Image backImg = new Image(FarmBot.class.getResourceAsStream("images/back.png"));
        //Image backImg = new Image(getClass().getResource("images/back.png").toExternalForm(), 30, 46, true, true);
        Button backBtn = new Button();
        backBtn.setGraphic(new ImageView(backImg));
        
        backBtn.setOnAction(e -> backToMain());
        
        HBox root = new HBox(40);
        root.getChildren().addAll(backBtn, titleText);
        reportScene = new Scene(root, 500, 500);
        
        primaryStage.setTitle("Farm Bot - Reports");
        primaryStage.setScene(reportScene);
        primaryStage.show();
    }
    
    /**
     * Go back to main UI window
     * 
     */
    public void backToMain(){
        FarmBot.getInstance().goToMain();
    }
}
