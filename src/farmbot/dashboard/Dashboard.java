package farmbot.dashboard;

import farmbot.FarmBot;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 * Dashboard UI
 * @author Nick
 */
public class Dashboard {
    
    static Scene dashboardScene;
    
    public void init(Stage primaryStage) {
        //Button btn = new Button();
        //btn.setText("self destruct farm");

        // title
        Text text1 = new Text("\nDashboard");
        text1.setFont(Font.font(24));

        // b1
        Button button1 = new Button("Reports"); // reports
        Image image1 = new Image(FarmBot.class.getResourceAsStream("images/piechart.png"));
        button1.setGraphic(new ImageView(image1));
        button1.setWrapText(true);
        button1.setMinSize(100, 100);
        button1.setMaxSize(100, 100);
        button1.setContentDisplay(ContentDisplay.TOP);

        // b2
        Button button2 = new Button("Livestock");
        Image image2 = new Image(FarmBot.class.getResourceAsStream("images/cow.png"));
        button2.setGraphic(new ImageView(image2));
        button2.setMinSize(100, 100);
        button2.setMaxSize(100, 100);
        button2.setContentDisplay(ContentDisplay.TOP);

        // b3
        Button button3 = new Button("Moisture");
        Image image3 = new Image(FarmBot.class.getResourceAsStream("images/rain.png"));
        button3.setGraphic(new ImageView(image3));
        button3.setMinSize(100, 100);
        button3.setMaxSize(100, 100);
        button3.setContentDisplay(ContentDisplay.TOP);

        // b4
        Button button4 = new Button("Soil");
        Image image4 = new Image(FarmBot.class.getResourceAsStream("images/soil.png"));
        button4.setGraphic(new ImageView(image4));
        button4.setMinSize(100, 100);
        button4.setMaxSize(100, 100);
        button4.setContentDisplay(ContentDisplay.TOP);

        // b5
        Button button5 = new Button("Crops");
        Image image5 = new Image(FarmBot.class.getResourceAsStream("images/plant.png"));
        button5.setGraphic(new ImageView(image5));
        button5.setMinSize(100, 100);
        button5.setMaxSize(100, 100);
        button5.setContentDisplay(ContentDisplay.TOP);

        // b6
        Button button6 = new Button("Performance");
        Image image6 = new Image(FarmBot.class.getResourceAsStream("images/tachometer.png"));
        button6.setGraphic(new ImageView(image6));
        button6.setMinSize(100, 100);
        button6.setMaxSize(100, 100);
        button6.setContentDisplay(ContentDisplay.TOP);

        // b7
        Button button7 = new Button("Pests"); // pests
        Image image7 = new Image(FarmBot.class.getResourceAsStream("images/bug.png"));
        button7.setGraphic(new ImageView(image7));
        button7.setMinSize(100, 100);
        button7.setMaxSize(100, 100);
        button7.setContentDisplay(ContentDisplay.TOP);

        // b8
        Button button8 = new Button("Survey"); // survey
        Image image8 = new Image(FarmBot.class.getResourceAsStream("images/videocamera.png"));
        button8.setGraphic(new ImageView(image8));
        button8.setMinSize(100, 100);
        button8.setMaxSize(100, 100);
        button8.setContentDisplay(ContentDisplay.TOP);

        // b9
        Button button9 = new Button("Settings"); // settings
        Image image9 = new Image(FarmBot.class.getResourceAsStream("images/settings.png"));
        button9.setGraphic(new ImageView(image9));
        button9.setMinSize(100, 100);
        button9.setMaxSize(100, 100);
        button9.setContentDisplay(ContentDisplay.TOP);

        // create gv (ROW POS, COLUMN POS, 1, 1)
        GridPane gridPane = new GridPane();

        gridPane.add(button1, 0, 0, 1, 1);
        gridPane.add(button2, 1, 0, 1, 1);
        gridPane.add(button3, 2, 0, 1, 1);

        gridPane.add(button4, 0, 1, 1, 1);
        gridPane.add(button5, 1, 1, 1, 1);
        gridPane.add(button6, 2, 1, 1, 1);

        gridPane.add(button7, 0, 2, 1, 1);
        gridPane.add(button8, 1, 2, 1, 1);
        gridPane.add(button9, 2, 2, 1, 1);

        // format gv
        gridPane.setMaxWidth(Double.MAX_VALUE);
        AnchorPane.setLeftAnchor(gridPane, 0.0);
        AnchorPane.setRightAnchor(gridPane, 0.0);
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setHgap(10);
        gridPane.setVgap(10);

        // example button action
        //button1.setOnAction(e -> window.setScene(Reports.getScene()));
        button1.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                Reports reports = new Reports();
                reports.init(FarmBot.getInstance().getMainScene());
            }
        });

        // FarmBot SCENE
        // vbox stacks on top of eachother, StackPane puts things in the center
        StackPane root = new StackPane();
        root.getChildren().addAll(text1, gridPane);
        StackPane.setAlignment(text1, Pos.TOP_CENTER);
        dashboardScene = new Scene(root, 500, 600);

        primaryStage.setTitle("Farm Bot");
        primaryStage.getIcons().add(new Image(FarmBot.class.getResourceAsStream("images/icon.png")));
        primaryStage.setScene(dashboardScene);
        primaryStage.show();

    }
    
    
    /**
     * Go back to main UI window
     * 
     */
    public void backToMain(){
        FarmBot.getInstance().goToMain();
    }
    
}
