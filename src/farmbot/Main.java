package farmbot;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 *
 * @author Nick
 */
public class Main extends Application {

    /**
     * Initialize the application instance and UI
     * 
     */
    @Override
    public void start(Stage primaryStage) {
        FarmBot.getInstance();
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
