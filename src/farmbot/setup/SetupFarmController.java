package farmbot.setup;

import farmbot.Component.Component;
import farmbot.Component.Item;
import farmbot.Component.ItemContainer;
import farmbot.FarmBot;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.cell.TextFieldTreeTableCell;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.util.converter.DoubleStringConverter;
import javafx.util.converter.IntegerStringConverter;

/**
 * FXML Controller class
 *
 * @author Nick
 */
public class SetupFarmController implements Initializable {

    @FXML
    private TreeTableView<Component> setupTable;
    @FXML
    private TreeTableColumn<Component, String> colName;
    @FXML
    private TreeTableColumn<Component, Double> colPrice;
    @FXML
    private TreeTableColumn<Component, Double> colX;
    @FXML
    private TreeTableColumn<Component, Double> colY;
    @FXML
    private TreeTableColumn<Component, Double> colLength;
    @FXML
    private TreeTableColumn<Component, Double> colWidth;
    
    private TreeItem<Component> rootItem;
    private ItemContainer root;
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        // test code for adding containers and items
        root = FarmBot.getInstance().getRoot();
//        ItemContainer barn = new ItemContainer("barn", 1000, 0, 0, 500, 500);
//        root.addItem(barn);
//        barn.addItem(new Item("Cow", 100, 250, 250, 5, 5));
//        barn.addItem(new Item("Chicken", 50, 300, 300, 2, 2));
//        ItemContainer box = new ItemContainer("Equipment Storage", 500, 10, 10, 200, 200);
//        barn.addItem(box);
        
        // set ItemContainer root to rootItem of tree
        rootItem = FarmBot.getInstance().getRootTreeItem();
        buildTree(root, rootItem);

        
        // tells the table where to get the values from
        colName.setCellValueFactory((param) -> new SimpleStringProperty(param.getValue().getValue().getName()));
        colPrice.setCellValueFactory((param) -> new SimpleDoubleProperty(param.getValue().getValue().getPrice()).asObject());
        colX.setCellValueFactory((param) -> new SimpleDoubleProperty(param.getValue().getValue().getLocationX()).asObject());
        colY.setCellValueFactory((param) -> new SimpleDoubleProperty(param.getValue().getValue().getLocationY()).asObject());
        colLength.setCellValueFactory((param) -> new SimpleDoubleProperty(param.getValue().getValue().getLength()).asObject());
        colWidth.setCellValueFactory((param) -> new SimpleDoubleProperty(param.getValue().getValue().getWidth()).asObject());
        
        // allow edit of name
        colName.setCellFactory(TextFieldTreeTableCell.forTreeTableColumn());
        colName.setOnEditCommit(t -> {
            TreeItem<Component> currEdited = setupTable.getTreeItem(t.getTreeTablePosition().getRow());
            currEdited.getValue().changeName(t.getNewValue());
        });
        
        // allow edit of price
        colPrice.setCellFactory(TextFieldTreeTableCell.forTreeTableColumn(new DoubleStringConverter()));
        colPrice.setOnEditCommit(t -> {
            TreeItem<Component> currEdited = setupTable.getTreeItem(t.getTreeTablePosition().getRow());
            currEdited.getValue().changePrice(t.getNewValue());
        });
        
        // allow edit of x location
        colX.setCellFactory(TextFieldTreeTableCell.forTreeTableColumn(new DoubleStringConverter()));
        colX.setOnEditCommit(t -> {
            TreeItem<Component> currEdited = setupTable.getTreeItem(t.getTreeTablePosition().getRow());
            currEdited.getValue().changeLocationX(t.getNewValue());
        });
        
        // allow edit of y location
        colY.setCellFactory(TextFieldTreeTableCell.forTreeTableColumn(new DoubleStringConverter()));
        colY.setOnEditCommit(t -> {
            TreeItem<Component> currEdited = setupTable.getTreeItem(t.getTreeTablePosition().getRow());
            currEdited.getValue().changeLocationY(t.getNewValue());
        });
        
        // allow edit of length
        colLength.setCellFactory(TextFieldTreeTableCell.forTreeTableColumn(new DoubleStringConverter()));
        colLength.setOnEditCommit(t -> {
            TreeItem<Component> currEdited = setupTable.getTreeItem(t.getTreeTablePosition().getRow());
            currEdited.getValue().changeLength(t.getNewValue());
        });
        
        // allow edit of width
        colWidth.setCellFactory(TextFieldTreeTableCell.forTreeTableColumn(new DoubleStringConverter()));
        colWidth.setOnEditCommit(t -> {
            TreeItem<Component> currEdited = setupTable.getTreeItem(t.getTreeTablePosition().getRow());
            currEdited.getValue().changeWidth(t.getNewValue());
        });
        
        // add rootItem to tree and hide it
        setupTable.setEditable(true);
        setupTable.setRoot(rootItem);
        setupTable.setShowRoot(false);
        
    }
    
    /**
     * Builds the setupTree from the list of Items and ItemContainers
     * @param root the Item/Container
     * @param child the Item/Container casted into a TreeItem
     */
    private void buildTree(Component root, TreeItem<Component> child) {
        if (root instanceof ItemContainer) {
            for (Component item: ((ItemContainer)root).getList()) {
                TreeItem<Component> child1 = new TreeItem<>(item);
                child.getChildren().add(child1);
                buildTree(item, child1);
            }
        }
        if (root instanceof Item) {
            //final TreeItem<Component> child2 = new TreeItem<>(root);
            //child.getChildren().add(child2);
        }
    }

    @FXML
    private void openItemCreator(ActionEvent event) {
        FarmBot.getInstance().setCurrentComponent(FarmBot.getInstance().getRoot());
        FarmBot.getInstance().setCurrentTreeItem(FarmBot.getInstance().getRootTreeItem());
        try {
            Parent root1 = (Parent) FXMLLoader.load(getClass().getResource("ItemCreator.fxml"));
            //Stage stage = FarmBot.getInstance().getMainScene(); // opens in same window
            Stage stage = new Stage(); // opens in new window/stage
            stage.setTitle("Farm Bot - New Item");
            stage.getIcons().add(new Image(FarmBot.class.getResourceAsStream("images/icon.png")));
            stage.setScene(new Scene(root1));
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @FXML
    private void openContainerCreator(ActionEvent event) {
        FarmBot.getInstance().setCurrentComponent(FarmBot.getInstance().getRoot());
        FarmBot.getInstance().setCurrentTreeItem(FarmBot.getInstance().getRootTreeItem());
        try {
            Parent root1 = (Parent) FXMLLoader.load(getClass().getResource("ContainerCreator.fxml"));
            //Stage stage = FarmBot.getInstance().getMainScene(); // opens in same window
            Stage stage = new Stage(); // opens in new window/stage
            stage.getIcons().add(new Image(FarmBot.class.getResourceAsStream("images/icon.png")));
            stage.setTitle("Farm Bot - New Container");
            stage.setScene(new Scene(root1));
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void onFinish(ActionEvent event) {
        // go to main
        //FarmBot.getInstance().goToMain();
        try {
            Parent root1 = (Parent) FXMLLoader.load(FarmBot.class.getResource("visualize/PlotFarm.fxml"));
            //Stage stage = FarmBot.getInstance().getMainScene(); // opens in same window
            Stage stage = new Stage(); // opens in new window/stage
            stage.setTitle("Farm Bot - Visualize - Display: (Name, X, Y, Length, Width)");
            stage.getIcons().add(new Image(FarmBot.class.getResourceAsStream("images/icon.png")));
            stage.setScene(new Scene(root1));
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void onDelete(ActionEvent event) {
        
        // instantiate component that is being deleted
        String name = (String)setupTable.getSelectionModel().getSelectedItem().getValue().getName();
        TreeItem t = (TreeItem)setupTable.getSelectionModel().getSelectedItem();
        Component c = (Component)setupTable.getSelectionModel().getSelectedItem().getValue();
        String type;
        if (c instanceof Item) {
            type = "an item";
        } else {
            type = "a container";
        }
        
        // ask user if they want to delete it
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm Delete");
        alert.setHeaderText("You are about to delete " + type + " called " + name);
        alert.setContentText("Are you sure you want to delete " + name + "?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){

            // delete from the tree (faster than reloading)
            boolean remove = t.getParent().getChildren().remove(t);

            // delete from the Container list
            root.removeItem(c);

            // reload the tree
            //rootItem = new TreeItem<>(root);
            //buildTree(root, rootItem);
            //setupTable.setRoot(rootItem);
            //setupTable.setShowRoot(false);
        } else {
            // do nothing
        }
        
    }

    @FXML
    private void onCreateContainer(ActionEvent event) {
        TreeItem t = (TreeItem)setupTable.getSelectionModel().getSelectedItem();
        Component c = (Component)setupTable.getSelectionModel().getSelectedItem().getValue();
        
        // if container
        if(c instanceof ItemContainer) {
            // set the currently selected container
            FarmBot.getInstance().setCurrentComponent(c);
            FarmBot.getInstance().setCurrentTreeItem(t);
        
            try {
                Parent root1 = (Parent) FXMLLoader.load(getClass().getResource("ContainerCreator.fxml"));
                //Stage stage = FarmBot.getInstance().getMainScene(); // opens in same window
                Stage stage = new Stage(); // opens in new window/stage
                stage.getIcons().add(new Image(FarmBot.class.getResourceAsStream("images/icon.png")));
                stage.setTitle("Farm Bot - New Container");
                stage.setScene(new Scene(root1));
                stage.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        // if item
        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error");
            alert.setHeaderText(null);
            alert.setContentText("You cannot add children to an Item");
            alert.showAndWait();
        }
    }

    @FXML
    private void onCreateItem(ActionEvent event) {
        TreeItem t = (TreeItem)setupTable.getSelectionModel().getSelectedItem();
        Component c = (Component)setupTable.getSelectionModel().getSelectedItem().getValue();
        
        // if container
        if(c instanceof ItemContainer) {
            // set the currently selected container
            FarmBot.getInstance().setCurrentComponent(c);
            FarmBot.getInstance().setCurrentTreeItem(t);
        
            try {
                Parent root1 = (Parent) FXMLLoader.load(getClass().getResource("ItemCreator.fxml"));
                //Stage stage = FarmBot.getInstance().getMainScene(); // opens in same window
                Stage stage = new Stage(); // opens in new window/stage
                stage.getIcons().add(new Image(FarmBot.class.getResourceAsStream("images/icon.png")));
                stage.setTitle("Farm Bot - New Item");
                stage.setScene(new Scene(root1));
                stage.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        // if item
        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error");
            alert.setHeaderText(null);
            alert.setContentText("You cannot add children to an Item");
            alert.showAndWait();
        }
    }
    
}
