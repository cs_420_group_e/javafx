package farmbot;

import farmbot.dashboard.Dashboard;
import farmbot.Component.Component;
import farmbot.Component.ItemContainer;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 *
 * @author Nick
 */
public class FarmBot{

    static FarmBot instance;
    private Stage window;
    Scene mainScene;
    
    
    ItemContainer root = new ItemContainer("root", 0, 0, 0, 100, 100);
    TreeItem<Component> rootItem = new TreeItem<>(root);
    TreeItem currentTreeItem;
    Component currentComponent;
    
    /**
     * Starts an application instance
     *
     */
    public FarmBot(){
        this.startApplication();
    }
    
     /**
     * Initialize the application instance and UI
     * 
     */
    private void startApplication() {
        window = new Stage();
        instance = this;
        
        // set window scene to initial setup
        try {
            Parent root1 = (Parent) FXMLLoader.load(getClass().getResource("setup/SetupFarm.fxml"));
            Stage stage = FarmBot.getInstance().getMainScene();
            stage.getIcons().add(new Image(FarmBot.class.getResourceAsStream("images/icon.png")));
            stage.setTitle("Farm Bot");
            stage.setScene(new Scene(root1));
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        // set window scene to dashboard
        //Dashboard dashboard = new Dashboard();
        //dashboard.init(window);

    }

    /**
     * Gets the instance of the application (Singleton)
     * @return the application instance
     */
    public static FarmBot getInstance() {
        if(instance == null){
            return new FarmBot();
        }
        return instance;
    }
    
    /**
     * 
     * @return the main application Scene
     */
    public Stage getMainScene() {
        return window;
    }
    
    /**
     * 
     * @return the ItemContainer root
     */
    public ItemContainer getRoot() {
        return root;
    }
    
    /**
     * 
     * @return the tree root item
     */
    public TreeItem<Component> getRootTreeItem() {
        return rootItem;
    }
    
    /**
     * 
     * @return the selected tree item
     */
    public TreeItem<Component> getCurrentTreeItem() {
        return currentTreeItem;
    }
    
    /**
     * 
     * Sets the currentComponent
     */
    public void setCurrentComponent(Component c) {
        currentComponent = c;
    }
    
    /**
     * 
     * Sets the currentTreeItem
     */
    public void setCurrentTreeItem(TreeItem<Component> t) {
        currentTreeItem = t;
    }
    
    /**
     * 
     * @return the selected component
     */
    public Component getCurrentComponent() {
        return currentComponent;
    }

    /**
     * set scene to main (Dashboard.java)
     * 
     */
    public void goToMain() {
        Dashboard dashboard = new farmbot.dashboard.Dashboard();
        dashboard.init(window);
        //window.setTitle("Farm Bot");
        //window.setScene(mainScene);
        //window.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
